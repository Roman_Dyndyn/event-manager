package com.dyndyn.eventmanager.exception;

/**
 * This exception will be thrown if EmailService encounter a problem
 *
 * @author Roman Dyndyn
 */
public class MailException extends RuntimeException {

    public MailException(Throwable cause) {
        super(cause);
    }
}
