package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for generic operations on a repository for a {@link VerificationToken} object.
 *
 * @author Roman Dyndyn
 */
@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

    VerificationToken findByToken(String token);

}
