package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for generic operations on a repository for a {@link Student} object.
 *
 * @author Roman Dyndyn
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    Student findByEmail(String email);

}
