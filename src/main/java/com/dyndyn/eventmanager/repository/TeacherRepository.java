package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for generic operations on a repository for a {@link Teacher} object.
 *
 * @author Roman Dyndyn
 */
@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    Teacher findByEmail(String email);

}
