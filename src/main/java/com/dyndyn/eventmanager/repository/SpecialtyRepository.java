package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.model.Specialty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for generic operations on a repository for a {@link Specialty} object.
 *
 * @author Roman Dyndyn
 */
@Repository
public interface SpecialtyRepository extends JpaRepository<Specialty, Long> {
}
