package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.dto.GroupDTO;
import com.dyndyn.eventmanager.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interface for generic operations on a repository for a {@link Group} object.
 *
 * @author Roman Dyndyn
 */
@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

    @Query("SELECT new com.dyndyn.eventmanager.dto.GroupDTO(g.id, g.name) FROM Group g WHERE g.name LIKE %:name%")
    List<GroupDTO> findAllByNameContains(@Param("name") String name);

}
