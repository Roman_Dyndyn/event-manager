package com.dyndyn.eventmanager.repository;

import com.dyndyn.eventmanager.model.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface for generic operations on a repository for a {@link Faculty} object.
 *
 * @author Roman Dyndyn
 */
@Repository
public interface FacultyRepository extends JpaRepository<Faculty, Long> {

}
