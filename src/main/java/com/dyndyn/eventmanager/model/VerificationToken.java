package com.dyndyn.eventmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "verification_tokens")
@Getter
@Setter
@NoArgsConstructor
public class VerificationToken implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;

    @OneToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @OneToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    private LocalDateTime expiryDate;

    public VerificationToken(String token, Student student, LocalDateTime expiryDate) {
        this.token = token;
        this.student = student;
        this.expiryDate = expiryDate;
    }

    public VerificationToken(String token, Teacher teacher, LocalDateTime expiryDate) {
        this.token = token;
        this.teacher = teacher;
        this.expiryDate = expiryDate;
    }

}
