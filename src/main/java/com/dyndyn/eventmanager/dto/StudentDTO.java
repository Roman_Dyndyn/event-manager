package com.dyndyn.eventmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class StudentDTO implements UserDTO {

    private long id;

    @Length(min = 3, max = 60)
    @NotBlank
    private String surname;

    @Length(min = 3, max = 60)
    @NotBlank
    private String name;

    @Length(min = 3, max = 60)
    @NotBlank
    private String patronymic;

    @Email
    private String email;

    @Length(min = 5, max = 60)
    @NotBlank
    private String password;

    @Length(min = 5, max = 60)
    @NotBlank
    private String confirmPassword;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    @Length(min = 12, max = 13)
    @NotBlank
    private String telephone;

    @Length(max = 255)
    @NotBlank
    private String address;

    private String groupName;

    private long groupId;

}
