package com.dyndyn.eventmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GroupDTO {

    private long id;
    private String name;
    private long facultyId;

    public GroupDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
