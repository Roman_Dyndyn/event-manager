package com.dyndyn.eventmanager.dto;

public interface UserDTO {

    String getEmail();
    String getPassword();
    String getConfirmPassword();

}
