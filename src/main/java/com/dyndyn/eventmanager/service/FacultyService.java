package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.model.Faculty;

import java.util.List;
import java.util.Optional;

public interface FacultyService {

    void add(Faculty faculty);

    Optional<Faculty> getById(long id);

    List<Faculty> getAll();

    void update(Faculty faculty);

    void remove(Faculty faculty);

}
