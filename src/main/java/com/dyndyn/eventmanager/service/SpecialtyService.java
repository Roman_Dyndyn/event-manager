package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.model.Specialty;

import java.util.List;
import java.util.Optional;

public interface SpecialtyService {

    void add(Specialty specialty);

    Optional<Specialty> getById(long id);

    List<Specialty> getAll();

    void update(Specialty specialty);

    void remove(Specialty specialty);
    
}
