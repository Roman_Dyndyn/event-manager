package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.dto.StudentDTO;
import com.dyndyn.eventmanager.model.Student;

import java.util.Optional;

public interface StudentService {

    Student register(StudentDTO accountDto);

    Optional<Student> getById(long id);

    Student getByEmail(String email);

    void update(Student student);

    void enable(String token);

    Student deleteToken(String token);
}
