package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.dto.TeacherDTO;
import com.dyndyn.eventmanager.model.Teacher;

import java.util.Optional;

public interface TeacherService {

    Teacher register(TeacherDTO accountDto);

    Optional<Teacher> getById(long id);

    Teacher getByEmail(String email);

    void update(Teacher teacher);

    void enable(String token);

    Teacher deleteToken(String token);
}
