package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.model.Student;
import com.dyndyn.eventmanager.model.Teacher;
import com.dyndyn.eventmanager.model.VerificationToken;
import com.dyndyn.eventmanager.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Transactional
public class TokenService {

    private final int expiration;
    private VerificationTokenRepository tokenRepository;

    public TokenService(@Value("${app.registration.token.expiration}") int expiration, VerificationTokenRepository tokenRepository) {
        this.expiration = expiration;
        this.tokenRepository = tokenRepository;
    }

    public VerificationToken create(Student student) {
        String token = UUID.randomUUID().toString();
        return tokenRepository.save(new VerificationToken(token, student, calculateExpiryDate()));
    }

    public VerificationToken create(Teacher teacher) {
        String token = UUID.randomUUID().toString();
        return tokenRepository.save(new VerificationToken(token, teacher, calculateExpiryDate()));
    }

    public VerificationToken renew(String token) {
        VerificationToken verificationToken = tokenRepository.findByToken(token);
        verificationToken.setToken(UUID.randomUUID().toString());
        verificationToken.setExpiryDate(calculateExpiryDate());
        tokenRepository.save(verificationToken);
        return verificationToken;
    }

    public VerificationToken get(String verificationToken) {
        return tokenRepository.findByToken(verificationToken);
    }

    public void delete(VerificationToken verificationToken) {
        tokenRepository.delete(verificationToken);
    }

    private LocalDateTime calculateExpiryDate() {
        return LocalDateTime.now().plus(Duration.ofHours(expiration));
    }
}
