package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.dto.TeacherDTO;
import com.dyndyn.eventmanager.exception.ExpiredTokenException;
import com.dyndyn.eventmanager.exception.InvalidTokenException;
import com.dyndyn.eventmanager.model.Role;
import com.dyndyn.eventmanager.model.Teacher;
import com.dyndyn.eventmanager.model.VerificationToken;
import com.dyndyn.eventmanager.repository.TeacherRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * The TeacherServiceImpl class is used to hold business logic for working with the {@link Teacher} object.
 *
 * @author Roman Dyndyn
 */
@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class TeacherServiceImpl implements TeacherService {

    private TeacherRepository teacherRepository;
    private TokenService tokenService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Teacher register(TeacherDTO accountDto) {
        log.info("Registering teacher with email {}", accountDto.getEmail());
        Teacher teacher = Teacher.builder().surname(accountDto.getSurname()).name(accountDto.getName()).patronymic(accountDto.getPatronymic())
                .birthday(accountDto.getBirthday()).telephone(accountDto.getTelephone()).address(accountDto.getAddress())
                .email(accountDto.getEmail().toLowerCase()).password(bCryptPasswordEncoder.encode(accountDto.getPassword())).role(Role.TEACHER).build();
        return teacherRepository.save(teacher);
    }

    @Override
    public Optional<Teacher> getById(long id) {
        return teacherRepository.findById(id);
    }

    @Override
    public Teacher getByEmail(String email) {
        return teacherRepository.findByEmail(email);
    }

    @Override
    public void update(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    @Override
    public void enable(String token) {
        VerificationToken verificationToken = tokenService.get(token);

        if (verificationToken == null) {
            log.debug("Invalid token {}", token);
            throw new InvalidTokenException();
        }

        Teacher teacher = verificationToken.getTeacher();
        if (verificationToken.getExpiryDate().isBefore(LocalDateTime.now())) {
            log.info("Token has been expired for student {}", teacher.getEmail());
            throw new ExpiredTokenException();
        }
        teacher.setEnabled(true);
        teacherRepository.save(teacher);
        tokenService.delete(verificationToken);
        log.info("Teacher {} has been enabled", teacher.getEmail());
    }

    @Override
    public Teacher deleteToken(String token) {
        VerificationToken verificationToken = tokenService.get(token);
        Teacher teacher = verificationToken.getTeacher();
        tokenService.delete(verificationToken);
        return teacher;
    }

}
