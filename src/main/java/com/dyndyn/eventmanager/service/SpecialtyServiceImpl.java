package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.model.Specialty;
import com.dyndyn.eventmanager.repository.SpecialtyRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The SpecialtyServiceImpl class is used to hold business logic for working with the {@link Specialty} object.
 *
 * @author Roman Dyndyn
 */
@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class SpecialtyServiceImpl implements SpecialtyService {

    private SpecialtyRepository specialtyRepository;

    @Override
    public void add(Specialty specialty) {
        specialtyRepository.save(specialty);
        log.info("Specialty {} has been added", specialty.getName());
    }

    @Override
    public Optional<Specialty> getById(long id) {
        return specialtyRepository.findById(id);
    }

    @Override
    public List<Specialty> getAll() {
        return specialtyRepository.findAll();
    }

    @Override
    public void update(Specialty specialty) {
        specialtyRepository.save(specialty);
        log.info("Specialty {} has been updated", specialty.getName());
    }

    @Override
    public void remove(Specialty specialty) {
        specialtyRepository.delete(specialty);
        log.info("Specialty {} has been removed", specialty.getName());
    }
}
