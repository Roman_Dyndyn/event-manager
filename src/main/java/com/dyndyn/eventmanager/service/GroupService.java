package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.dto.GroupDTO;
import com.dyndyn.eventmanager.model.Group;

import java.util.List;
import java.util.Optional;

public interface GroupService {

    void add(Group group);

    Optional<Group> getById(long id);

    List<GroupDTO> find(String name);

    List<Group> getAll();

    void update(Group group);

    void remove(Group group);
    
}
