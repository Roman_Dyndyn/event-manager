package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.model.Faculty;
import com.dyndyn.eventmanager.repository.FacultyRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The FacultyServiceImpl class is used to hold business logic for working with the {@link Faculty} object.
 *
 * @author Roman Dyndyn
 */
@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class FacultyServiceImpl implements FacultyService {

    private FacultyRepository facultyRepository;

    @Override
    public void add(Faculty faculty) {
        facultyRepository.save(faculty);
        log.info("Faculty {} has been added", faculty.getName());
    }

    @Override
    public Optional<Faculty> getById(long id) {
        return facultyRepository.findById(id);
    }

    @Override
    public List<Faculty> getAll() {
        return facultyRepository.findAll();
    }

    @Override
    public void update(Faculty faculty) {
        facultyRepository.save(faculty);
        log.info("Faculty {} has been updated", faculty.getName());
    }

    @Override
    public void remove(Faculty faculty) {
        facultyRepository.delete(faculty);
        log.info("Faculty {} has been removed", faculty.getName());
    }
}
