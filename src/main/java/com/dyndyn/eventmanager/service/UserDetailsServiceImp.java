package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.repository.StudentRepository;
import com.dyndyn.eventmanager.repository.TeacherRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class UserDetailsServiceImp implements UserDetailsService {

    private StudentRepository studentRepository;
    private TeacherRepository teacherRepository;


    @Override
    public UserDetails loadUserByUsername(String username) {
        username = username.toLowerCase();
        UserDetails userDetails = studentRepository.findByEmail(username);
        if (userDetails != null) {
            return userDetails;
        }

        userDetails = teacherRepository.findByEmail(username);
        if (userDetails != null) {
            return userDetails;
        }

        log.info("A user with email {} is not found", username);
        throw new UsernameNotFoundException("No user found with username: " + username);
    }

}
