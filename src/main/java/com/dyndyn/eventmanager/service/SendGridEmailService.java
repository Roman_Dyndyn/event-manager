package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.exception.MailException;
import com.sendgrid.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * The SendGridEmailService class is used to send email using SendGrid.
 *
 * @author Roman Dyndyn
 */
@Service
@Slf4j
public class SendGridEmailService implements EmailService {

    private SendGrid sendGridClient;
    private String from;

    public SendGridEmailService(SendGrid sendGridClient, @Value("${app.from.email}") String from) {
        this.sendGridClient = sendGridClient;
        this.from = from;
    }

    @Override
    public void send(String to, String subject, String body) {
        send(from, to, subject, body);
    }

    /**
     * Sends an email with content type 'text/plain'.
     *
     * @param from the email's from address.
     * @param to the email's recipient.
     * @param subject the email's subject line.
     * @param body the email's content.
     */
    @Override
    public void send(String from, String to, String subject, String body) {
        Response response = sendEmail(from, to, subject, new Content("text/plain", body));
        log.info("Status Code: " + response.getStatusCode() + ", Body: " + response.getBody() + ", Headers: "
                + response.getHeaders());
    }

    private Response sendEmail(String from, String to, String subject, Content content) {
        Mail mail = new Mail(new Email(from), subject, new Email(to), content);
        Request request = new Request();
        Response response;

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            response = this.sendGridClient.api(request);
        } catch (IOException e) {
            throw new MailException(e);
        }

        return response;
    }
}
