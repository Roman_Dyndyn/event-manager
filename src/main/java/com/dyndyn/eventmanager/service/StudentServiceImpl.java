package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.dto.StudentDTO;
import com.dyndyn.eventmanager.exception.ExpiredTokenException;
import com.dyndyn.eventmanager.exception.InvalidTokenException;
import com.dyndyn.eventmanager.model.*;
import com.dyndyn.eventmanager.repository.StudentRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * The StudentServiceImpl class is used to hold business logic for working with the {@link Student} object.
 *
 * @author Roman Dyndyn
 */
@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private TokenService tokenService;

    @Override
    public Student register(StudentDTO accountDto) {
        log.info("Registering Student with email {}", accountDto.getEmail());
        Group group = new Group();
        group.setId(accountDto.getGroupId());

        Student.StudentBuilder builder = Student.builder().surname(accountDto.getSurname()).name(accountDto.getName()).patronymic(accountDto.getPatronymic())
                .birthday(accountDto.getBirthday()).telephone(accountDto.getTelephone()).address(accountDto.getAddress())
                .email(accountDto.getEmail().toLowerCase()).password(bCryptPasswordEncoder.encode(accountDto.getPassword())).role(Role.STUDENT)
                .group(group);
        return studentRepository.save(builder.build());
    }

    @Override
    public Optional<Student> getById(long id) {
        return studentRepository.findById(id);
    }

    @Override
    public Student getByEmail(String email) {
        return studentRepository.findByEmail(email);
    }

    @Override
    public void update(Student student) {
        studentRepository.save(student);
    }

    @Override
    public void enable(String token) {
        VerificationToken verificationToken = tokenService.get(token);

        if (verificationToken == null) {
            log.debug("Invalid token {}", token);
            throw new InvalidTokenException();
        }

        Student student = verificationToken.getStudent();
        if (verificationToken.getExpiryDate().isBefore(LocalDateTime.now())) {
            log.info("Token has been expired for student {}", student.getEmail());
            throw new ExpiredTokenException();
        }
        student.setEnabled(true);
        studentRepository.save(student);
        tokenService.delete(verificationToken);
        log.info("Student {} has been enabled", student.getEmail());
    }

    @Override
    public Student deleteToken(String token) {
        VerificationToken verificationToken = tokenService.get(token);
        Student student = verificationToken.getStudent();
        tokenService.delete(verificationToken);
        return student;
    }


}
