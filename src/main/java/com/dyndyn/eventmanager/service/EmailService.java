package com.dyndyn.eventmanager.service;

public interface EmailService {
    void send(String to, String subject, String body);
    void send(String from, String to, String subject, String body);
}