package com.dyndyn.eventmanager.service;

import com.dyndyn.eventmanager.dto.GroupDTO;
import com.dyndyn.eventmanager.model.Group;
import com.dyndyn.eventmanager.repository.GroupRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The GroupServiceImpl class is used to hold business logic for working with the {@link Group} object.
 *
 * @author Roman Dyndyn
 */
@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class GroupServiceImpl implements GroupService {

    private GroupRepository groupRepository;

    @Override
    public void add(Group group) {
        groupRepository.save(group);
        log.info("Group {} has been added", group.getName());
    }

    @Override
    public Optional<Group> getById(long id) {
        return groupRepository.findById(id);
    }

    @Override
    public List<GroupDTO> find(String name) {
        List<GroupDTO> groups = groupRepository.findAllByNameContains(name);
        log.info("Found {} groups which names contain the '{}'", groups.size(), name);
        return groups;
    }

    @Override
    public List<Group> getAll() {
        return groupRepository.findAll();
    }

    @Override
    public void update(Group group) {
        groupRepository.save(group);
        log.info("Group {} has been updated", group.getName());
    }

    @Override
    public void remove(Group group) {
        groupRepository.delete(group);
        log.info("Group {} has been removed", group.getName());
    }
}
