package com.dyndyn.eventmanager.config;

import com.sendgrid.SendGrid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for SendGrid integration
 *
 * @author Roman Dyndyn
 */
@Configuration
public class SendGridConfig {

    /**
     * Creates {@link SendGrid} bean that allows access to the SendGrid API.
     */
    @Bean
    public SendGrid sendGrid(@Value("${sendgrid.api.key}") String sendGridAPIKey){
        return new SendGrid(sendGridAPIKey);
    }
}
