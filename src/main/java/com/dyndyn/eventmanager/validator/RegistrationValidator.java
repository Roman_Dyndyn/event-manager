package com.dyndyn.eventmanager.validator;

import com.dyndyn.eventmanager.dto.UserDTO;
import com.dyndyn.eventmanager.service.StudentService;
import com.dyndyn.eventmanager.service.TeacherService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validates the user input from Registration form
 *
 * @author Roman Dyndyn
 */

@Component
@AllArgsConstructor
public class RegistrationValidator implements Validator {

    private TeacherService teacherService;
    private StudentService studentService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDTO userDTO = (UserDTO) target;
        if (!userDTO.getPassword().equals(userDTO.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "validation.password.different");

        }
        if (studentService.getByEmail(userDTO.getEmail()) != null || teacherService.getByEmail(userDTO.getEmail()) != null) {
            errors.rejectValue("email", "validation.email.exist");
        }

    }
}
