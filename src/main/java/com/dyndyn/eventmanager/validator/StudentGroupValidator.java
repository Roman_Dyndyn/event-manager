package com.dyndyn.eventmanager.validator;

import com.dyndyn.eventmanager.dto.StudentDTO;
import com.dyndyn.eventmanager.model.Group;
import com.dyndyn.eventmanager.service.GroupService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

/**
 * Validates the user input from Registration form
 *
 * @author Roman Dyndyn
 */

@Component
@AllArgsConstructor
public class StudentGroupValidator implements Validator {

    private GroupService groupService;

    @Override
    public boolean supports(Class<?> clazz) {
        return StudentDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        StudentDTO studentDTO = (StudentDTO) target;
        Optional<Group> group = groupService.getById(studentDTO.getGroupId());
        if (!group.filter(g -> g.getName().equals(studentDTO.getGroupName())).isPresent()) {
            errors.rejectValue("groupName", "validation.registration.group.incorrect");
        }
    }
}
