package com.dyndyn.eventmanager.controller;

import com.dyndyn.eventmanager.dto.StudentDTO;
import com.dyndyn.eventmanager.event.OnRegistrationCompleteEvent;
import com.dyndyn.eventmanager.model.Student;
import com.dyndyn.eventmanager.service.StudentService;
import com.dyndyn.eventmanager.validator.RegistrationValidator;
import com.dyndyn.eventmanager.validator.StudentGroupValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

@Controller
@RequestMapping("/students")
@Slf4j
public class StudentController {
    private ApplicationEventPublisher applicationEventPublisher;
    private StudentService studentService;
    private MessageSource messageSource;
    private RegistrationValidator registrationValidator;
    private StudentGroupValidator studentValidator;


    public StudentController(ApplicationEventPublisher applicationEventPublisher, StudentService studentService,
                             MessageSource messageSource, RegistrationValidator registrationValidator,
                             StudentGroupValidator studentValidator) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.studentService = studentService;
        this.messageSource = messageSource;
        this.registrationValidator = registrationValidator;
        this.studentValidator = studentValidator;
    }


    @PostMapping
    public String register(@Validated @ModelAttribute("student") StudentDTO student, BindingResult result, WebRequest request, RedirectAttributes redirectAttributes) {
        registrationValidator.validate(student, result);
        studentValidator.validate(student, result);
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.student", result);
            redirectAttributes.addFlashAttribute("student", student);
            log.debug("Validation error for student {}", student.getEmail());
            return "redirect:/register";
        }

        Student registered = studentService.register(student);
        applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getContextPath(), request.getLocale()));

        return "successRegister";
    }

    @GetMapping("/confirm-email/{token}")
    public String confirmEmail(@PathVariable String token) {
        studentService.enable(token);
        return "redirect:/login";
    }


    @GetMapping("/resend-token/{token}")
    public String resendRegistrationToken(WebRequest request, Map<String, Object> model, @PathVariable String token) {
        applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(studentService.deleteToken(token), request.getContextPath(), request.getLocale()));
        model.put("message", messageSource.getMessage("registration.success", null, request.getLocale()));

        return "successRegister";
    }

}
