package com.dyndyn.eventmanager.controller;

import com.dyndyn.eventmanager.dto.StudentDTO;
import com.dyndyn.eventmanager.dto.TeacherDTO;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collections;
import java.util.Map;

@Controller
public class HomeController {

    @GetMapping("/")
    public String welcome(Map<String, Object> model){
        return "index";
    }

    @GetMapping("/login")
    public String login(){
        SecurityContextHolder.clearContext();
        return "login";
    }



    @GetMapping("/register")
    public String register(Map<String, Object> model){
        model.putIfAbsent("teacher", new TeacherDTO());
        model.putIfAbsent("student", new StudentDTO());
        model.put("groups", Collections.emptyList());
        model.put("specialties", Collections.emptyList());
        return "register";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "error/403";
    }

}
