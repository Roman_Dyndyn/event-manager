package com.dyndyn.eventmanager.controller;

import com.dyndyn.eventmanager.dto.TeacherDTO;
import com.dyndyn.eventmanager.event.OnRegistrationCompleteEvent;
import com.dyndyn.eventmanager.model.Teacher;
import com.dyndyn.eventmanager.service.TeacherService;
import com.dyndyn.eventmanager.validator.RegistrationValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

@Controller
@RequestMapping("/teachers")
@Slf4j
public class TeacherController {

    private ApplicationEventPublisher applicationEventPublisher;
    private TeacherService teacherService;
    private MessageSource messageSource;
    private RegistrationValidator validator;

    public TeacherController(ApplicationEventPublisher applicationEventPublisher, TeacherService teacherService,
                             MessageSource messageSource, RegistrationValidator validator) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.teacherService = teacherService;
        this.messageSource = messageSource;
        this.validator = validator;
    }

    @PostMapping
    public String register(@Validated @ModelAttribute("teacher") TeacherDTO teacher, BindingResult result, WebRequest request, RedirectAttributes redirectAttributes) {
        validator.validate(teacher, result);
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.teacher", result);
            redirectAttributes.addFlashAttribute("teacher", teacher);
            log.info("Validation error for teacher {}", teacher.getEmail());
            return "redirect:/register";
        }

        Teacher registered = teacherService.register(teacher);
        applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getContextPath(), request.getLocale()));

        return "redirect:/login";
    }

    @GetMapping("/confirm-email/{token}")
    public String confirmEmail(@PathVariable String token) {
        teacherService.enable(token);
        return "redirect:/login";
    }

    @GetMapping("/resend-token/{token}")
    public String resendRegistrationToken(WebRequest request, Map<String, Object> model, @PathVariable("token") String token) {
        applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(teacherService.deleteToken(token), request.getContextPath(), request.getLocale()));
        model.put("message", messageSource.getMessage("registration.success", null, request.getLocale()));

        return "successRegister";
    }

}
