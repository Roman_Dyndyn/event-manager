package com.dyndyn.eventmanager.controller;

import com.dyndyn.eventmanager.dto.GroupDTO;
import com.dyndyn.eventmanager.service.GroupService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/groups")
@AllArgsConstructor
public class GroupController {

    private GroupService groupService;

    @GetMapping("/name/{term}")
    public List<GroupDTO> findByName(@PathVariable String term) {
        return groupService.find(term);
    }

}
