package com.dyndyn.eventmanager.controller;

import com.dyndyn.eventmanager.service.SpecialtyService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/specialties")
@AllArgsConstructor
public class SpecialtyController {

    private SpecialtyService specialtyService;

}
