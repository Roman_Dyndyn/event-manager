package com.dyndyn.eventmanager.listener;

import com.dyndyn.eventmanager.event.OnRegistrationCompleteEvent;
import com.dyndyn.eventmanager.model.VerificationToken;
import com.dyndyn.eventmanager.service.EmailService;
import com.dyndyn.eventmanager.service.TokenService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 * Sends email for confirming email address of a newly created user.
 *
 * @author Roman Dyndyn
 */
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private TokenService tokenService;
    private MessageSource messageSource;
    private EmailService emailService;
    private String appUrl;
    private String port;

    public RegistrationListener(TokenService tokenService, MessageSource messageSource, EmailService emailService,
                                @Value("${app.url}") String appUrl, @Value("${server.port}")String port) {
        this.tokenService = tokenService;
        this.messageSource = messageSource;
        this.emailService = emailService;
        this.appUrl = appUrl;
        this.port = port;
    }

    /**
     * Handle an {@link OnRegistrationCompleteEvent} event.
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        VerificationToken verificationToken;
        String recipientAddress;
        if (event.getStudent() != null) {
            verificationToken = tokenService.create(event.getStudent());
            recipientAddress = event.getStudent().getEmail();
        } else {
            verificationToken = tokenService.create(event.getTeacher());
            recipientAddress = event.getTeacher().getEmail();
        }
        String subject = messageSource.getMessage("registration.email.subject", null, event.getLocale());
        String message = messageSource.getMessage("registration.email.text",
                new Object[]{createConfirmationUrl(event.getContextPath(), verificationToken)}, event.getLocale());

        emailService.send(recipientAddress, subject, message);
    }

    private String createConfirmationUrl(String contextPath, VerificationToken token) {
        StringBuilder stringBuilder = new StringBuilder(appUrl).append(':').append(port).append(contextPath).append('/');
        if (token.getStudent() != null){
            stringBuilder.append("students");
        } else {
            stringBuilder.append("teachers");
        }
        stringBuilder.append("/confirm-email/").append(token.getToken());
        return stringBuilder.toString();
    }
}
