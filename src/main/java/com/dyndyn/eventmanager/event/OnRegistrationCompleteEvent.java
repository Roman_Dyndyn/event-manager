package com.dyndyn.eventmanager.event;

import com.dyndyn.eventmanager.model.Student;
import com.dyndyn.eventmanager.model.Teacher;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

/**
 * An event that occurs after registration.
 *
 * @author Roman Dyndyn
 */
@Getter
@Setter
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private String contextPath;
    private Locale locale;
    private Student student;
    private Teacher teacher;

    /**
     * Create a new OnRegistrationCompleteEvent.
     *
     * @param student newly created {@link Student}
     * @param contextPath  application URL
     * @param locale  locale of new user
     */
    public OnRegistrationCompleteEvent(Student student, String contextPath, Locale locale) {
        super(student);
        this.contextPath = contextPath;
        this.locale = locale;
        this.student = student;
    }

    /**
     * Create a new OnTeacherRegistrationCompleteEvent.
     *
     * @param teacher newly created {@link Teacher}
     * @param contextPath application URL
     * @param locale locale of new user
     */
    public OnRegistrationCompleteEvent(Teacher teacher, String contextPath, Locale locale) {
        super(teacher);
        this.contextPath = contextPath;
        this.locale = locale;
        this.teacher = teacher;
    }
}
