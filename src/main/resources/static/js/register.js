$(function () {

    $('#student-form-link').click(function (e) {
        $("#student-form").delay(100).fadeIn(100);
        $("#teacher-form").fadeOut(100);
        $('#teacher-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#teacher-form-link').click(function (e) {
        $("#teacher-form").delay(100).fadeIn(100);
        $("#student-form").fadeOut(100);
        $('#student-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });


    $('#student-group').autocomplete({
        source: function (request, response) {
            $.getJSON("/groups/name/" + request.term, null, function (result) {
                response($.map(result, function (item) {
                    return {
                        // following property gets displayed in drop down
                        label: item.name,
                        // following property gets entered in the textbox
                        value: item.id
                    }
                }));
            });
        },
        select: function (event, ui) {
            if (ui.item) {
                event.preventDefault();
                $('#student-group').val(ui.item.label);
                $('#student-group-id').val(ui.item.value);
            }
        }

    });

});
